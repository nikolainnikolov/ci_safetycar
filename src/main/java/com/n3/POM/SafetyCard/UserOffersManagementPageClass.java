package com.n3.POM.SafetyCard;

import com.n3.WD.support.Dropdown;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class UserOffersManagementPageClass  extends  PageBase{


    private static final String offerStatus = "//div[@class='row']//div[1]//div[1]//div[1]//span[1]";
    private static final String carBrandMoel = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h3[1]";
    private static final String engineCubicCapacity = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h4[1]";
    private static final String viewCar_Reg_Cer = "//div[@class='row']//div[1]//div[1]//div[1]//h4[3]//a[1]";
    private static final String INS_EffectiveDate = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h4[4]";
    private static final String HadAccident_status = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h4[5]";
    private static final String ageOfTheDrier = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h4[6]";
    private static final String totalPremiumAmount = "/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h4[6]";
    private static final String cancelOfferButton = "//div[@class='row']//div[1]//div[1]//div[1]//div[1]//form[1]//button[1]";


    private WebDriver driver;
    private Dropdown dropdown;
    public UserOffersManagementPageClass(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public void offerStatusIsCorrect(String expectedOfferStatus) throws InterruptedException {
        Thread.sleep(777);
        WebDriverWait wait = getWait();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(offerStatus)));
        driver.findElement(By.xpath("//div[@class='row']//div[1]//div[1]//div[1]//span[1]")).isDisplayed();
        String actualOfferStatus = driver.findElement(By.xpath("//div[@class='row']//div[1]//div[1]//div[1]//span[1]")).getText();
        Assert.assertEquals(actualOfferStatus,expectedOfferStatus);
    }

    public void carBrandModelIsCorrect(String expectedCarManDate) {

        WebDriverWait wait = getWait();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(carBrandMoel)));
        //just for debugging reasons
        driver.findElement(By.xpath(carBrandMoel)).isDisplayed();

        String os = driver.findElement(By.xpath(carBrandMoel)).getText();
        System.out.println(os);
        Assert.assertEquals(os,expectedCarManDate);
    }


}
