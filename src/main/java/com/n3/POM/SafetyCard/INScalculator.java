package com.n3.POM.SafetyCard;

import com.n3.WD.support.Dropdown;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class INScalculator extends PageBase {

    //Xpath Selectors
    private static final String INScalc_carBrandDD  =  "//select[@id='carBrand']";
    private static final String INScalc_carModeDD   = "//select[@id='carModel']";
    private static final String INScalc_capacityDD  = "//input[@id='cubic-capacity-field']";
    private static final String INScalc_firstRegDateFIELD   = "//input[@id='firstRegDate']";
    private static final String INScalc_isAccidentHappen_CheckBOX   = "//input[@id='offer.hadAccident1']";
    private static final String INScalc_driverAge   = "//input[@id='driver-age-id']";
    private static final String INScalc_calucalte_button   = "//button[@class='login100-form-btn calculate']";

    private WebDriver driver;
    private Dropdown dropdown;
    public INScalculator(WebDriver driver){
        super(driver);
        this.driver = driver;
    }


    @FindBy(how = How.XPATH,using = INScalc_carBrandDD)
    public WebElement carBrand_DropDown;

    @FindBy(how = How.XPATH,using =INScalc_carModeDD)
    public WebElement carModel_DropDown;

    @FindBy(how = How.XPATH,using = INScalc_capacityDD)
    public WebElement capacity_FIELD;

    @FindBy(how = How.XPATH,using = INScalc_firstRegDateFIELD)
    public WebElement car_RegistrationDate_FIELD;

    @FindBy(how = How.XPATH,using = INScalc_isAccidentHappen_CheckBOX)
    public WebElement isAccidentHappen_CheckBox;

    @FindBy(how = How.XPATH,using = INScalc_driverAge)
    public WebElement driverAge;

    @FindBy(how = How.XPATH,using = INScalc_calucalte_button)
    public WebElement calculate_submit_BTN;

    @FindBy(how = How.XPATH,using = "//strong[contains(text(),'Total Premium:')]")
    public WebElement premium_txt_header;


    public void select_car_Brand  (String carBrand) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(INScalc_carBrandDD))));


        driver.findElement(By.xpath(INScalc_carBrandDD)).click();

        WebElement selectedCar = driver.findElement(By.xpath("//select[@id='carBrand']//option[contains(text(),'"+carBrand+"')]"));
        selectedCar.click();

        Thread.sleep(1500);


//       WebDriverWait wait = getWait();
//       wait.until(ExpectedConditions.visibilityOf(carBrand_DropDown));
//
//       List<WebElement> carMakers = driver.findElements(By.cssSelector("select#carBrand>option"));
//
//        for (WebElement carBrand:carMakers) {
//            if (carBrand.getText().trim().equals(carMaker)) {
//
//                carBrand.click();
//                String selectedBrand = carBrand.getText();
//                System.out.println("The selected car brand is: "+selectedBrand);
//            }
////            System.out.println(carBrand.getText());
////            System.out.println("==================");
//        }



    }


    public  void select_car_model(String carModel) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(INScalc_carModeDD))));


        driver.findElement(By.xpath(INScalc_carModeDD)).click();

        WebElement selectedCar = driver.findElement(By.xpath("//select[@id='carModel']//option[contains(text(),'"+carModel+"')]"));
        selectedCar.click();

        Thread.sleep(1500);
    }


    public void provide_car_cubic_capacity(String capacity) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(capacity_FIELD));

        capacity_FIELD.clear();
        capacity_FIELD.sendKeys(capacity);

        Thread.sleep(1500);
    }

    public void enter_first_registration_date(String date) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(car_RegistrationDate_FIELD));

        date.replace("/","");
        car_RegistrationDate_FIELD.clear();
        car_RegistrationDate_FIELD.sendKeys(date);

        Thread.sleep(1500);
    }

    public void select_isCarAccidentHappened (String isSelected) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(isAccidentHappen_CheckBox));

        if (isSelected.trim().toLowerCase().equals("yes")) {

            isAccidentHappen_CheckBox.click();

        }

        Thread.sleep(1500);
    }


    public void provide_drivers_age (String age) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(driverAge));

        driverAge.clear();
        driverAge.sendKeys(age);

    }


    public void check_premium_amount(String amount) throws InterruptedException {
        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(premium_txt_header));

        driver.findElement(By.xpath("//strong[contains(text(),'"+amount+"')]"));

        Thread.sleep(1500);
    }



}
