package com.n3.POM.SafetyCard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RequestOfferPageClass extends PageBase{

    private WebDriver driver;

    public RequestOfferPageClass(WebDriver driver) {
        super(driver);
        this.driver = driver;

    }
    public static final String ins_start_date = "//input[@id='effectiveDate']";
    public static final String upload_car_reg_cert =  "//input[@id='docimage']";
//    public static final String upload_car_reg_cert = "//label[@class='custom-file-upload login100-form-btn']";

    public static final String request_offer_BTN = "//button[@class='login100-form-btn']";


    public void enter_insurance_startDate(String date) throws InterruptedException {

        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(ins_start_date))));

        date.replace("/","");
        driver.findElement(By.xpath(ins_start_date)).clear();
        driver.findElement(By.xpath(ins_start_date)).sendKeys(date);

        Thread.sleep(888);
    }

    public void upload_certificate(String filePath) throws InterruptedException {
        WebDriverWait wait = getWait();
        wait.until(ExpectedConditions.presenceOfElementLocated( By.xpath(upload_car_reg_cert)));

//        WebElement frame =driver.switchTo().activeElement();
//        frame.sendKeys("src/test/resources/VINnumber.jpg");

        driver.findElement(By.xpath(upload_car_reg_cert)).sendKeys(filePath);


    }

    public void click_on_request_offer_btn () {

        driver.findElement(By.xpath(request_offer_BTN)).click();

    }
}
