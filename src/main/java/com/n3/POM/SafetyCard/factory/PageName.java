package com.n3.POM.SafetyCard.factory;

public enum PageName {
	HomePage,
	LoginPage,
	RegistrationPage,
	INScalc,
	OfferPage,
	UserOffersManagementPage

}
