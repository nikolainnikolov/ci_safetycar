package com.n3.POM.SafetyCard.factory;


import com.n3.POM.SafetyCard.*;
import org.openqa.selenium.WebDriver;



public class CustomFactory {
	

	
	public static PageBase getInstance(PageName name, WebDriver driver) {
		
		switch (name) {
			case HomePage:
				return new HomePageClass(driver);
			case LoginPage:
				return new LoginPageClass(driver);
			case RegistrationPage:
				return new RegistrationPageClass(driver);
			case INScalc:
				return new INScalculator(driver);
			case OfferPage:
				return new RequestOfferPageClass(driver);

			case UserOffersManagementPage:
				return new UserOffersManagementPageClass(driver);

		default:
			throw new RuntimeException("Invalid Page Name");
		}
		
	}

}
