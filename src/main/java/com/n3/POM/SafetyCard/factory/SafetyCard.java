package com.n3.POM.SafetyCard.factory;


import com.n3.POM.SafetyCard.*;

public class SafetyCard {
	
	public HomePageClass homepage;
	public LoginPageClass loginPage;
	public RegistrationPageClass registrationPage;
	public INScalculator INScalc;
	public RequestOfferPageClass offerPage;
	public UserOffersManagementPageClass UserOffersManagementPage;

	public PageBase page;
	public String data = "";

}
