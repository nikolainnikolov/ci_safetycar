package com.n3.Cucumber;

import com.n3.WD.support.DriverSvS;
import org.openqa.selenium.WebDriver;



import cucumber.api.Scenario;
import cucumber.api.java.*;


public class GeneralHook {
	
	private DriverSvS services;
	private WebDriver driver;
	
	public GeneralHook(DriverSvS services) {
		this.services = services;
		this.driver = services.getDriver();
	}
	
	@Before(value="@regression",order = 2)
	public void setupForRegression(){
		System.out.println("Regression suite");
	}
	
	@Before(value="@smoke")
	public void setupForSmoke(){
		System.out.println("Smoke suite");
	}
	
	@Before(order = 1)
	public void setup() {
		System.out.println("Test suite");
	}
	
	@After
	public void teardown(Scenario scenario) {
		if(scenario.isFailed()){
			captureScreenShot(scenario);
		}
		if(driver != null){
	    	driver.quit(); // it will close all the window and stop the web driver
	    }
		
	}

	private void captureScreenShot(Scenario scenario) {
		int random = (int)(Math.random() * 1000);
		services.getScreenShotController().takeScrenShot("target/report.json", "src" + random +".png");
		scenario.embed(services.getScreenShotController().takeScrenShot(), "image/png");
	}

}
