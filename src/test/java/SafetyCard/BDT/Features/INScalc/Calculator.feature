Feature: Safety card insurance calculator
          As a SafetyCard user,
          I want to be able to do calculate a insurance premium
          so that I can use inform myself for the amount
          and use this for further actions within the Website

  Scenario Outline: Calculate an insurance premium by providing valid data

    Given  The user launches the SafetyCard calculator page with URL "<url>"
    When The user selects a car brand from a dropDown menu as "<car>"
    When The user selects a car model from the dropDown menu as "<model>"
    When The user enters car cubic capacity "<capacity>"
    When The user provides date for the car first registration date "<regDate>"
    When The user select the 'Had accident check' box "<isAccidentHappened>"
    When The user provides information for the driver`s age "<driverAge>"
    And The user clicks on calculate submit button
    Then A car insurance is presented with total amount "<premiumAmount>"

    Examples:
    |url| car|model|capacity|regDate|isAccidentHappened|driverAge|premiumAmount|
    |http://localhost:8080/calculator| Audi| A4|600| 11/11/2011|no|44|443.58 BGN|
    |http://localhost:8080/calculator| Audi| A4|700| 11/11/2011|yes|44|532.29 BGN|
    |http://localhost:8080/calculator| Audi| A4|800| 11/11/2011|yes|19|554.47 BGN|
    |http://localhost:8080/calculator| Audi| A4|900| 11/11/1999|no|44|454.58 BGN|
    |http://localhost:8080/calculator| Audi| A4|1000| 11/11/1999|yes|44|545.49 BGN|
    |http://localhost:8080/calculator| Audi| A4|1047| 11/11/1999|yes|19|568.22 BGN|
#    Category 2
    |http://localhost:8080/calculator| Audi| A4|1048| 11/11/2011|no|44|582.59 BGN|
    |http://localhost:8080/calculator| Audi| A4|1100| 11/11/2011|yes|44|699.11 BGN|
    |http://localhost:8080/calculator| Audi| A4|1150| 11/11/2011|yes|19|728.24 BGN|
    |http://localhost:8080/calculator| Audi| A4|1200| 11/11/1999|no|44|593.59 BGN|
    |http://localhost:8080/calculator| Audi| A4|1250| 11/11/1999|yes|44|712.31 BGN|
    |http://localhost:8080/calculator| Audi| A4|1309| 11/11/1999|yes|19|741.99 BGN|
#    Category 3
    |http://localhost:8080/calculator| Audi| A4|1310| 11/11/2011|no|44|760.06 BGN|
    |http://localhost:8080/calculator| Audi| A4|1470| 11/11/2011|yes|44|912.07 BGN|
    |http://localhost:8080/calculator| Audi| A4|1500| 11/11/2011|yes|19|950.07 BGN|
    |http://localhost:8080/calculator| Audi| A4|1680| 11/11/1999|no|44|771.06 BGN|
    |http://localhost:8080/calculator| Audi| A4|1709| 11/11/1999|yes|44|925.27 BGN|
    |http://localhost:8080/calculator| Audi| A4|2356| 11/11/1999|yes|19|963.82 BGN|
#    Category 4
    |http://localhost:8080/calculator| Audi| A4|2357| 11/11/2011|no|44|949.15 BGN|
    |http://localhost:8080/calculator| Audi| A4|2400| 11/11/2011|yes|44|1138.98 BGN|
    |http://localhost:8080/calculator| Audi| A4|2500| 11/11/2011|yes|19|1186.43 BGN|
    |http://localhost:8080/calculator| Audi| A4|2600| 11/11/1999|no|44|982.15 BGN|
    |http://localhost:8080/calculator| Audi| A4|2700| 11/11/1999|yes|44|1178.58 BGN|
    |http://localhost:8080/calculator| Audi| A4|2880| 11/11/1999|yes|19|1227.68 BGN|
  #    Category 5
    |http://localhost:8080/calculator| Audi| A4|2881| 11/11/2011|no|44|1053.68 BGN|
    |http://localhost:8080/calculator| Audi| A4|3000| 11/11/2011|yes|44|1264.41 BGN|
    |http://localhost:8080/calculator| Audi| A4|3200| 11/11/2011|yes|19|1317.10 BGN|
    |http://localhost:8080/calculator| Audi| A4|3400| 11/11/1999|no|44|1086.68 BGN|
    |http://localhost:8080/calculator| Audi| A4|3600| 11/11/1999|yes|44|1304.01 BGN|
    |http://localhost:8080/calculator| Audi| A4|4188| 11/11/1999|yes|19|1358.35 BGN|
#    Category 6
    |http://localhost:8080/calculator| Audi| A4|4189| 11/11/2011|no|44|1184.28 BGN|
    |http://localhost:8080/calculator| Audi| A4|4200| 11/11/2011|yes|44|1421.14 BGN|
    |http://localhost:8080/calculator| Audi| A4|4300| 11/11/2011|yes|19|1480.35 BGN|
    |http://localhost:8080/calculator| Audi| A4|4400| 11/11/1999|no|44|1217.28 BGN|
    |http://localhost:8080/calculator| Audi| A4|5200| 11/11/1999|yes|44|1460.74 BGN|
    |http://localhost:8080/calculator| Audi| A4|5497| 11/11/1999|yes|19|1521.60 BGN|
#    Category 7
    |http://localhost:8080/calculator| Audi| A4|5498| 11/11/2011|no|44|1312.58 BGN|
    |http://localhost:8080/calculator| Audi| A4|5821| 11/11/2011|yes|44|1575.09 BGN|
    |http://localhost:8080/calculator| Audi| A4|64025| 11/11/2011|yes|19|1640.72 BGN|
    |http://localhost:8080/calculator| Audi| A4|72537| 11/11/1999|no|44|1389.58 BGN|
    |http://localhost:8080/calculator| Audi| A4|80955| 11/11/1999|yes|44|1667.49 BGN|
    |http://localhost:8080/calculator| Audi| A4|999999| 11/11/1999|yes|19|1736.97 BGN|

  Scenario Outline:Validate error messages are displayed for not selected option from the "Car maker" and "Car model" dropDown menu

    Given  The user launches the SafetyCard calculator page with URL "<url>"
    When The user selects/not selects a car brand from a dropDown menu as "<car>"
    When The user selects/not selects a car model from the dropDown menu as "<model>"
    When The user enters valid/invalid car cubic capacity "<capacity>"
    When The user provides valid/invalid date for the car first registration date "<regDate>"
    When The user select/not selects the 'Had accident check' box "<isCBselected>"
    When The user provides valid/invalid information for the driver`s age "<driverAge>"
    And The user clicks on calculate submit button
    Then An error message is presented "<errorMessage>"

    Examples:
      |url| car|model|capacity|regDate|isCBselected|driverAge|errorMessage|
      |http://localhost:8080/calculator|  |  |2000| 11/11/2011|yes|33| asdasd|

  Scenario Outline:Validate error message is displayed for not selected option from the "Car model" dropDown menu

    Given  The user launches the SafetyCard calculator page with URL "<url>"
    When The user selects/not selects a car brand from a dropDown menu as "<car>"
    When The user selects/not selects a car model from the dropDown menu as "<model>"
    When The user enters valid/invalid car cubic capacity "<capacity>"
    When The user provides valid/invalid date for the car first registration date "<regDate>"
    When The user select/not selects the 'Had accident check' box "<isCBselected>"
    When The user provides valid/invalid information for the driver`s age "<driverAge>"
    And The user clicks on calculate submit button
    Then An error message is presented "<errorMessage>"

    Examples:
      |url| car|model|capacity|regDate|isCBselected|driverAge|errorMessage|
      |http://localhost:8080/calculator| Audi |  |2000| 11/11/2011|yes|33| asdasd|


  Scenario Outline:Validate error messages are displayed for not selected option from the "Cubic capacity" input field

  Given  The user launches the SafetyCard calculator page with URL "<url>"
  When The user selects/not selects a car brand from a dropDown menu as "<car>"
  When The user selects/not selects a car model from the dropDown menu as "<model>"
  When The user enters valid/invalid car cubic capacity "<capacity>"
  When The user provides valid/invalid date for the car first registration date "<regDate>"
  When The user select/not selects the 'Had accident check' box "<isCBselected>"
  When The user provides valid/invalid information for the driver`s age "<driverAge>"
  And The user clicks on calculate submit button
  Then An error message is presented "<errorMessage>"

#  Failed to convert property value of type java.lang.String to required type java.lang.Integer for property offer.cubicCapacity; nested exception is java.lang.NumberFormatException: For input string: "2000cc"
  Examples:
    |url| car|model|capacity|regDate|isCBselected|driverAge|errorMessage|
    |http://localhost:8080/calculator| Audi  | A4 |  | 11/11/2011|yes|33| Please fill this field|
    |http://localhost:8080/calculator| Audi  | A4 | 2| 11/11/2011|yes|33| Invalid cubic capacity input|
    |http://localhost:8080/calculator| Audi  | A4 | 2000cc| 11/11/2011|yes|33|Invalid cubic capacity input|
    |http://localhost:8080/calculator| Audi  | A4 | 33.3| 11/11/2011|yes|33| Invalid cubic capacity input|
    |http://localhost:8080/calculator| Audi  | A4 | 123456789| 11/11/2011|yes|33| Invalid cubic capacity input|


  Scenario Outline:Validate error messages are displayed for not selected option from the "First registration" input field

    Given  The user launches the SafetyCard calculator page with URL "<url>"
    When The user selects/not selects a car brand from a dropDown menu as "<car>"
    When The user selects/not selects a car model from the dropDown menu as "<model>"
    When The user enters valid/invalid car cubic capacity "<capacity>"
    When The user provides valid/invalid date for the car first registration date "<regDate>"
    When The user select/not selects the 'Had accident check' box "<isCBselected>"
    When The user provides valid/invalid information for the driver`s age "<driverAge>"
    And The user clicks on calculate submit button
    Then An error message is presented "<errorMessage>"

#  Failed to convert property value of type java.lang.String to required type java.lang.Integer for property offer.cubicCapacity; nested exception is java.lang.NumberFormatException: For input string: "2000cc"
    Examples:
      |url| car|model|capacity|regDate|isCBselected|driverAge|errorMessage|
      |http://localhost:8080/calculator| Audi  | A4 | 2000| 02/30/2011|yes|33| Please fill this field|
      |http://localhost:8080/calculator| Audi  | A4 | 2000| 01/11/2113|yes|33|First Registration date must be today or in the past|
      |http://localhost:8080/calculator| Audi  | A4 | 2000| 01/11/2020|yes|33| Invalid first registration date|
      |http://localhost:8080/calculator| Audi  | A4 | 2000| 11/11/685|yes|33 | Invalid first registration date|


