Feature: LGN SafetyCard
        As a registered user,
        I want to be able to login in SafetyCard
        so that I can use the web-application features.

  @Smoke
  Scenario Outline:Client login with valid username and password

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is successfully logged-in and redirected to the home page
    Then User`s name is presented at navigation bar as link for a dropDown menu


    Examples:
      | url| userName|password|
      |http://localhost:8080/login | N3TestUser | Test123!|



  @Smoke
  Scenario Outline:Admin login with valid username and password

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is successfully logged-in and redirected to the home page
    Then User`s name is presented at navigation bar as link for a dropDown menu


    Examples:
      | url| userName|password|
      |http://localhost:8080/login | admin45 | p@ssword|

  @Regression
  Scenario Outline: Login-logout

    Given The user launches the SafetyCard home page with URL "<url>"
    When The user clicks at the navigation bar login link
    Then The user is redirected to the login page
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is successfully logged-in and redirected to the home page
    Then User`s name is presented at navigation bar as link for a dropDown menu


    Examples:
      | url| userName|password|
      |http://localhost:8080| N3TestUser | Test123!|


  @Regression
  Scenario Outline:Successful login after unsuccessful attempt

    Given The user launches the SafetyCard login page with URL "<url>" after unsuccessful login attempt
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is successfully logged-in and redirected to the home page
    Then User`s name is presented at navigation bar as link for a dropDown menu

    Examples:
      | url| userName|password|
      |http://localhost:8080/login | N3TestUser  | Test123! |
      |http://localhost:8080/login | admin45  | p@ssword |


  @Regression
  Scenario Outline:Not able to log in with blank userName and correct password

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is not successfully logged-in
    Then An error message is displayed as "<error_message>"

    Examples:
      | url| userName|password|error_message |
      |http://localhost:8080/login |   | Test123! |Bad credentials|


  @Regression
  Scenario Outline:Not able to log in with correct userName and blank password

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is not successfully logged-in
    Then An error message is displayed as "<error_message>"


    Examples:
      | url| userName|password|error_message |
      |http://localhost:8080/login |  N3TestUser  | |Bad credentials|


  @Regression
  Scenario Outline:Not able to log in with both blank username and password

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is not successfully logged-in
    Then An error message is displayed as "<error_message>"


    Examples:
      | url| userName|password|error_message |
      |http://localhost:8080/login |  | |Bad credentials|



  @Regression
  Scenario Outline:Not able to log in with disabled user

    Given The user launches the SafetyCard login page with URL "<url>"
    When The user enters username as "<userName>"
    When The user enters password as "<password>"
    And The user clicks on the login form submit button
    Then The user is not successfully logged-in
    Then An error message is displayed as "<error_message>"


    Examples:
      | url| userName|password| error_message|
      |http://localhost:8080/login | disabledUser | Test123! |User is disabled|



