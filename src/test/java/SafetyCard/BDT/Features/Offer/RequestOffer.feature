Feature: As a guest user i would like to have a feature to request an offer with already calculated premium

Scenario Outline: Request an offer with valid information

  Given The logged in client launches the SafetyCard calculator page with URL "<url>"
  When The user selects a car brand from a dropDown menu as "<car>"
  When The user selects a car model from the dropDown menu as "<model>"
  When The user enters car cubic capacity "<capacity>"
  When The user provides date for the car first registration date "<regDate>"
  When The user select the 'Had accident check' box "<isAccidentHappened>"
  When The user provides information for the driver`s age "<driverAge>"
  And The user clicks on calculate submit button
  Then A car insurance is presented with total amount "<premiumAmount>"
  Then Generate offer button is shown
  When The user clicks on the generate offer button
  Then An 'Request an offer' new windows is shown
  When The user enters insurance starting date as "<startInsuranceDate>"
  When The user upload the vehicle registration certificate
  When The user clicks on request an offer button
  Then User`s 'My request' page is presented
  Then Requested offer is with status "<offerStatus>"


  Examples:
|url| car|model|capacity|regDate|isAccidentHappened|driverAge|premiumAmount|startInsuranceDate|offerStatus|
|http://localhost:8080/calculator| Audi| A4|600| 11/11/2011|no|44|443.58 BGN| 03/03/2020      | Pending   |
#|http://localhost:8080/calculator| Audi| A4|700| 11/11/2011|yes|44|532.29 BGN|
#|http://localhost:8080/calculator| Audi| A4|800| 11/11/2011|yes|19|554.47 BGN|
#|http://localhost:8080/calculator| Audi| A4|900| 11/11/1999|no|44|454.58 BGN|
#|http://localhost:8080/calculator| Audi| A4|1000| 11/11/1999|yes|44|545.49 BGN|
#|http://localhost:8080/calculator| Audi| A4|1047| 11/11/1999|yes|19|568.22 BGN|
