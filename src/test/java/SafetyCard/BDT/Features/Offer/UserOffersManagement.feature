Feature: As a guest user i would like to have a feature to request an offer with already calculated premium

Scenario Outline: Request an offer with valid information

  Given A logged-in client with not managed by Administrator offer launches the SafetyCard client`s requests page with URL "<url>"
  Then The requested offer status is pending "<offerStatus>"
  Then The car and brand offer`s parameter in the offer is "<carBrand>"
  Then The car manufacturing date offer`s parameter  is "<CMD>"
  Then The engine cubic capacity offer`s parameter is "<capacity>"
  Then The offer`s parameter for effective date is "<insuranceStartDate>"



  Examples:
    |url| offerStatus|carBrand|CMD|capacity|insuranceStartDate|
    |http://localhost:8080/requests|Pending| Audi A4|11/11/2011|600cc|3/3/2020|

