package SafetyCard.BDT.StepDefinitions.REG;

import com.n3.POM.SafetyCard.RegistrationPageClass;
import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.WD.support.DriverSvS;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Registration_stepDefinitions {

    private WebDriver driver;
    private DriverSvS services;
    private SafetyCard safetyCard;

    private String password;

    public Registration_stepDefinitions(DriverSvS services, SafetyCard safetyCard) {
        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;

    }

    @Given("^The user launches the SafetyCard SignUp page with URL \"([^\"]*)\"$")
    public void the_user_launches_the_SafetyCard_SignUp_page_with_URL(String url) throws Throwable {
        driver.get(url);
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle,"Register");
        safetyCard.registrationPage = new RegistrationPageClass(driver);

    }

    @When("^The user writes First name as \"([^\"]*)\"$")
    public void the_user_writes_First_name_as(String name) throws Throwable {
        safetyCard.registrationPage.enter_first_name(name);
    }

    @When("^The user writes Last name as \"([^\"]*)\"$")
    public void the_user_writes_Last_name_as(String lastName) throws Throwable {
        safetyCard.registrationPage.enter_last_name(lastName);
    }

    @When("^The user writes email as \"([^\"]*)\"$")
    public void the_user_writes_email_as(String email) throws Throwable {
        String add_random_string = RandomStringUtils.randomAlphanumeric(3);
        safetyCard.registrationPage.enter_email(add_random_string+email);
    }

    @When("^The user writes phone as \"([^\"]*)\"$")
    public void the_user_writes_phone_as(String phone) throws Throwable {
        safetyCard.registrationPage.enter_phone(phone);
    }

    @When("^The user writes address as \"([^\"]*)\"$")
    public void the_user_writes_address_as(String address) throws Throwable {
        safetyCard.registrationPage.enter_address(address);
        Thread.sleep(3000);

    }

    @When("^The user write username as \"([^\"]*)\"$")
    public void the_user_write_username_as(String userName) throws Throwable {
        String add_random_string = RandomStringUtils.randomAlphanumeric(3);
        safetyCard.registrationPage.enter_userName(userName+add_random_string);
    }

    @When("^The user write password as \"([^\"]*)\"$")
    public void the_user_write_password_as(String pass) throws Throwable {
        safetyCard.registrationPage.enter_password(pass);
        password = pass;

    }

    @When("^The user confirm tha password$")
    public void the_user_confirm_tha_password() throws Throwable {
        safetyCard.registrationPage.confirm_password(password);
    }

    @When("^The user clicks on signUp button$")
    public void the_user_clicks_on_signUp_button() throws Throwable {
        safetyCard.registrationPage.click_on_signUP_button();
    }

    @Then("^The user is successfully registered at the SafetyCard$")
    public void the_user_is_successfully_registered_at_the_SafetyCard() throws Throwable {
        String currentURL = driver.getCurrentUrl();
        Assert.assertEquals(currentURL,"http://localhost:8080");
    }

    @Then("^The user is redirected to the home page$")
    public void the_user_is_redirected_to_the_home_page() throws Throwable {
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        String currentURL = driver.getCurrentUrl();
        Assert.assertEquals(currentURL,"http://localhost:8080/");
    }


    @When("^The user confirm tha password as \"([^\"]*)\"$")
    public void theUserConfirmThaPasswordAs(String pass) throws Throwable {
        safetyCard.registrationPage.REG_confirmPass_FIELD.sendKeys(pass);
    }

    @Then("^Then an error message will be presented as \"([^\"]*)\"$")
    public void thenAnErrorMessageWillBePresentedAs(String expected_errorMSG) throws Throwable {

        String error_msg_Xpath= "//span[contains(text(),'"+expected_errorMSG+"')]";

        WebDriverWait wait =getWait();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(error_msg_Xpath))));

        String getErrorMSGtxt = driver.findElement(By.xpath(error_msg_Xpath)).getText();
        System.out.println(getErrorMSGtxt);
        Assert.assertEquals(getErrorMSGtxt,expected_errorMSG);

        Thread.sleep(4000);

    }


    protected WebDriverWait getWait(){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.pollingEvery(250, TimeUnit.MILLISECONDS);
        wait.ignoring(NoSuchElementException.class);
        return wait;
    }
}
