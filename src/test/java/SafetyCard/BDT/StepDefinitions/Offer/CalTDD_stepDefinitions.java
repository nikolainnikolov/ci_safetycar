package SafetyCard.BDT.StepDefinitions.Offer;

import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.WD.support.DriverSvS;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class CalTDD_stepDefinitions {

    private WebDriver driver;
    private DriverSvS services;
    private WebDriverWait wait;
    private SafetyCard safetyCard;

    public CalTDD_stepDefinitions(DriverSvS services, SafetyCard safetyCard) {
        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;
    }



    @When("^The user selects/not selects a car brand from a dropDown menu as \"([^\"]*)\"$")
    public void the_user_selects_not_selects_a_car_brand_from_a_dropDown_menu_as(String car) throws Throwable {
        safetyCard.INScalc.select_car_Brand(car);

        List<WebElement> C_maker = driver.findElements(By.cssSelector("#carBrand>option"));

        for (WebElement car_maker:C_maker
             ) {
            System.out.println(car_maker);
        }
    }

    @When("^The user selects/not selects a car model from the dropDown menu as \"([^\"]*)\"$")
    public void the_user_selects_not_selects_a_car_model_from_the_dropDown_menu_as(String model) throws Throwable {
        safetyCard.INScalc.select_car_model(model);
    }

    @When("^The user enters valid/invalid car cubic capacity \"([^\"]*)\"$")
    public void the_user_enters_valid_invalid_car_cubic_capacity(String cc) throws Throwable {
        safetyCard.INScalc.provide_car_cubic_capacity(cc);
    }

    @When("^The user provides valid/invalid date for the car first registration date \"([^\"]*)\"$")
    public void the_user_provides_valid_invalid_date_for_the_car_first_registration_date(String date    ) throws Throwable {
        safetyCard.INScalc.enter_first_registration_date(date);
    }

    @When("^The user select/not selects the 'Had accident check' box \"([^\"]*)\"$")
    public void the_user_select_not_selects_the_Had_accident_check_box(String isSelected) throws Throwable {
        safetyCard.INScalc.select_isCarAccidentHappened(isSelected);
    }

    @When("^The user provides valid/invalid information for the driver`s age \"([^\"]*)\"$")
    public void the_user_provides_valid_invalid_information_for_the_driver_s_age(String age) throws Throwable {
        safetyCard.INScalc.provide_drivers_age(age);
    }

    @Then("^An error message is presented \"([^\"]*)\"$")
    public void an_error_message_is_presented(String expectedError ) throws Throwable {

        String error_msg_Xpath= "//span[contains(text(),'"+expectedError+"')]";

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(error_msg_Xpath))));

        String getErrorMSGtxt = driver.findElement(By.xpath(error_msg_Xpath)).getText();
        System.out.println(getErrorMSGtxt);
        Assert.assertEquals(getErrorMSGtxt,expectedError);

        Thread.sleep(4000);
//        driver.findElement(By.xpath("//span[contains(text(),'Invalid cubic capacity input')]"))
    }
}
