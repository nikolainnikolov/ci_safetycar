package SafetyCard.BDT.StepDefinitions.Offer;

import com.n3.POM.SafetyCard.INScalculator;
import com.n3.POM.SafetyCard.RequestOfferPageClass;
import com.n3.POM.SafetyCard.PageBase;
import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.WD.support.DriverSvS;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class Request_offer_stepDefinitions {
    private WebDriver driver;
    private DriverSvS services;
    private WebDriverWait wait;
    private SafetyCard safetyCard;

    public Request_offer_stepDefinitions(DriverSvS services, SafetyCard safetyCard) {
        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;
    }


    @Given("^The logged in client launches the SafetyCard calculator page with URL \"([^\"]*)\"$")
    public void the_logged_in_client_launches_the_SafetyCard_calculator_page_with_URL(String url) throws Throwable {
        safetyCard.page = new PageBase(driver);
        safetyCard.page.logIN("N3TestUser", "Test123!");
        driver.get(url);
        safetyCard.INScalc = new INScalculator(driver);
        System.out.println(driver.findElement(By.xpath("//span[@class='login100-form-title p-b-70 col-md-8']")).toString());

    }

    @Then("^Generate offer button is shown$")
    public void generate_offer_button_is_shown() throws Throwable {
      WebElement genOfferBTN = driver.findElement(By.xpath("//button[@class='login100-form-btn scroll']"));
        System.out.println(genOfferBTN.isDisplayed());
        genOfferBTN.click();


    }

    @When("^The user clicks on the generate offer button$")
    public void the_user_clicks_on_the_generate_offer_button() throws Throwable {
        WebElement genOfferBTN = driver.findElement(By.xpath("//button[@class='login100-form-btn scroll']"));
        genOfferBTN.click();
        services.getWindow().switchToWindow(1);
        safetyCard.offerPage = new RequestOfferPageClass(driver);

    }

    @Then("^An 'Request an offer' new windows is shown$")
    public void an_request_an_offer_new_windows_is_shown() throws Throwable {
        System.out.println(driver.getCurrentUrl());
    }

    @When("^The user enters insurance starting date as \"([^\"]*)\"$")
    public void the_user_enters_insurance_starting_date_as(String date) throws Throwable {
       safetyCard.offerPage = new RequestOfferPageClass(driver);
       safetyCard.offerPage.enter_insurance_startDate(date);
    }

    @When("^The user upload the vehicle registration certificate$")
    public void the_user_upload_the_vehicle_registration_certificate() throws Throwable {
       safetyCard.offerPage.upload_certificate("D:\\CODES\\M6\\V1_infoCucs\\src\\test\\resources\\VINnumber.jpg");
    }

    @When("^The user clicks on request an offer button$")
    public void the_user_clicks_on_request_an_offer_button() throws Throwable {
        safetyCard.offerPage.click_on_request_offer_btn();
        Thread.sleep(5000);
    }


    @Then("^User`s 'My request' page is presented$")
    public void userSMyRequestPageIsPresented() {

       String pageURL = driver.getCurrentUrl();

       Assert.assertEquals(pageURL,"http://localhost:8080/requests");
    }

    @Then("^Requested offer is with status \"([^\"]*)\"$")
    public void requestedOfferIsWithStatus(String arg0) throws Throwable {

        List<WebElement> user_requeted_offers = driver.findElements(By.cssSelector("div.well"));

        for (WebElement offer:user_requeted_offers) {
            
        }
    }
}
