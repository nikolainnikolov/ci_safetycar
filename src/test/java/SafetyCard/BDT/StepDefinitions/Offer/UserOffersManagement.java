package SafetyCard.BDT.StepDefinitions.Offer;

import com.n3.POM.SafetyCard.UserOffersManagementPageClass;
import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.WD.support.DriverSvS;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.OffsetDateTime;

public class UserOffersManagement {

    private WebDriver driver;
    private DriverSvS services;
    private WebDriverWait wait;
    private SafetyCard safetyCard;

    public UserOffersManagement(DriverSvS services, SafetyCard safetyCard) {
        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;
    }

    @Given("^A logged-in client with not managed by Administrator offer launches the SafetyCard client`s requests page with URL \"([^\"]*)\"$")
    public void a_client_with_not_managed_by_Administrator_offer_launches_the_SafetyCard_client_s_requests_page_with_URL(String url) throws Throwable {

    driver.get(url);
    safetyCard.UserOffersManagementPage = new UserOffersManagementPageClass(driver);
    safetyCard.UserOffersManagementPage.logIN("N3TestUser","Test123!");

    String currentURL = driver.getCurrentUrl();
    
    }

    @Then("^The requested offer status is pending \"([^\"]*)\"$")
    public void the_requested_offer_status_is_pending(String expectedOfferStatus) throws Throwable {
        Thread.sleep(777);

        driver.findElement(By.xpath("//div[@class='row']//div[1]//div[1]//div[1]//span[1]")).isDisplayed();
        String actualOfferStatus = driver.findElement(By.xpath("//div[@class='row']//div[1]//div[1]//div[1]//span[1]")).getText();
        Assert.assertEquals(actualOfferStatus,expectedOfferStatus);

    }

    @Then("^The car and brand offer`s parameter in the offer is \"([^\"]*)\"$")
    public void the_car_and_brand_offer_s_parameter_in_the_offer_is(String carBrand) throws Throwable {
        driver.findElement(By.xpath("/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h3[1]")).isDisplayed();
        String actualOfferStatus = driver.findElement(By.xpath("/html[1]/body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h3[1]")).getText();
        Assert.assertEquals(actualOfferStatus,carBrand);
    }

    @Then("^The car manufacturing date offer`s parameter  is \"([^\"]*)\"$")
    public void the_car_manufacturing_date_offer_s_parameter_is(String arg1) throws Throwable {
        Thread.sleep(7000);
    }

    @Then("^The engine cubic capacity offer`s parameter is \"([^\"]*)\"$")
    public void the_engine_cubic_capacity_offer_s_parameter_is(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^The offer`s parameter for effective date is \"([^\"]*)\"$")
    public void the_offer_s_parameter_for_effective_date_is(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
