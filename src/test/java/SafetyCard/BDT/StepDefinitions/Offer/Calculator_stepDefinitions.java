package SafetyCard.BDT.StepDefinitions.Offer;

import com.n3.POM.SafetyCard.INScalculator;
import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.WD.support.DriverSvS;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.Keys.PAGE_DOWN;

public class Calculator_stepDefinitions {

    private WebDriver driver;
    private DriverSvS services;
    private WebDriverWait wait;
    private SafetyCard safetyCard;

    public Calculator_stepDefinitions(DriverSvS services, SafetyCard safetyCard) {
        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;
    }


    @Given("^The user launches the SafetyCard calculator page with URL \"([^\"]*)\"$")
    public void the_user_launches_the_SafetyCard_calculator_page_with_URL(String url) throws Throwable {


        safetyCard.INScalc = new INScalculator(driver);

        driver.get(url);
        String pageTitle = driver.getTitle();
        String currURL = driver.getCurrentUrl();

        Assert.assertEquals(pageTitle, "Insurance Calculator");
        Assert.assertEquals(currURL, "http://localhost:8080/calculator");

    }


    @When("^The user selects a car brand from a dropDown menu as \"([^\"]*)\"$")
    public void the_user_selects_a_car_brand_from_a_dropDown_menu_as(String car) throws Throwable {

        safetyCard.INScalc.select_car_Brand(car);

    }

    @When("^The user selects a car model from the dropDown menu as \"([^\"]*)\"$")
    public void the_user_selects_a_car_model_from_the_dropDown_menu_as(String model) throws Throwable {

        safetyCard.INScalc.select_car_model(model);
    }

    @When("^The user enters car cubic capacity \"([^\"]*)\"$")
    public void the_user_enters_car_cubic_capacity(String cc) throws Throwable {
        safetyCard.INScalc.provide_car_cubic_capacity(cc);
    }

    @When("^The user provides date for the car first registration date \"([^\"]*)\"$")
    public void the_user_provides_date_for_the_car_first_registration_date(String date) throws Throwable {
        safetyCard.INScalc.enter_first_registration_date(date);
    }


    @When("^The user select the 'Had accident check' box \"([^\"]*)\"$")
    public void theUserSelectTheHadAccidentCheckBox(String isSelected) throws Throwable {
        safetyCard.INScalc.select_isCarAccidentHappened(isSelected);
    }

    @When("^The user provides information for the driver`s age \"([^\"]*)\"$")
    public void the_user_provides_information_for_the_driver_s_age(String age) throws Throwable {
        safetyCard.INScalc.provide_drivers_age(age);
    }

    @When("^The user clicks on calculate submit button$")
    public void the_user_clicks_on_calculate_submit_button() throws Throwable {
        safetyCard.INScalc.calculate_submit_BTN.click();
        Thread.sleep(1400);
    }

    @Then("^A car insurance is presented with total amount \"([^\"]*)\"$")
    public void a_car_insurance_is_presented_with_total_amount(String expectedAmount) throws Throwable {
        Actions action = new Actions(driver);
        action.sendKeys(PAGE_DOWN);
        String presentedAmount = driver.findElement(By.xpath("/html[1]/body[1]/div[3]/div[2]/strong[2]")).getText();

        System.out.println(presentedAmount);

        safetyCard.INScalc.check_premium_amount(expectedAmount);

        Assert.assertEquals(presentedAmount, expectedAmount);

    }


    @When("^Print me all values from the 'Car Make' drop-Down menu$")
    public void print_me_all_values_from_the_Car_Make_drop_Down_menu() throws Throwable {

        List<WebElement> carMakers = driver.findElements(By.cssSelector("#carBrand>option"));


        for (WebElement carMaker : carMakers
        ) {

            System.out.println("====+++ CAR MAKER +++===");
            System.out.println(carMaker.getText());

            carMaker.click();

            List<WebElement> carBrandOptions = driver.findElements(By.cssSelector("#carModel>option"));

            for (WebElement carBrandOption : carBrandOptions
            ) {
                System.out.println("________________");
                System.out.println(carBrandOption.getText());
//                if (carBrandOption.getText().equals("Other " + carMaker + " Models"))


            }

        }




    }
}






