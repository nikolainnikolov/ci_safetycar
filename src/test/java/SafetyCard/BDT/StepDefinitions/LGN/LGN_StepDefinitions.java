package SafetyCard.BDT.StepDefinitions.LGN;

import com.n3.POM.SafetyCard.HomePageClass;
import com.n3.POM.SafetyCard.LoginPageClass;
import com.n3.POM.SafetyCard.factory.SafetyCard;
import com.n3.TDD.transform.ExcelDataToDataTable;
import com.n3.WD.support.DriverSvS;
import com.n3.WD.support.WaitTool;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class LGN_StepDefinitions {

    private WebDriver driver;
    private DriverSvS services;
    private SafetyCard safetyCard;
    private String userName;

    public LGN_StepDefinitions (DriverSvS services, SafetyCard safetyCard) {

        this.services = services;
        this.driver = services.getDriver();
        this.safetyCard = safetyCard;
    }

    @Given("^The user launches the SafetyCard login page with URL \"([^\"]*)\"$")
    public void the_user_launches_the_SafetyCard_login_page_with_URL(String url) throws Throwable {
        driver.get(url);
        safetyCard.loginPage = new LoginPageClass(driver);

    }

    @When("^The user enters username as \"([^\"]*)\"$")
    public void the_user_enters_username_as(String user) throws Throwable {
        Thread.sleep(2000);

        safetyCard.loginPage = new LoginPageClass(driver);

        safetyCard.loginPage.user_name_field.click();
        userName=user;
//        safetyCard.loginPage.enterTextInInputFieldThourghJavaScript("#username-field", "blablabla");

        safetyCard.loginPage.enterUser(user);
//        String txtINfield =  safetyCard.loginPage.user_name_field.getAttribute("value");
//        System.out.println("The username is filled with text: "+txtINfield);
    }

    @When("^The user enters password as \"([^\"]*)\"$")
    public void the_user_enters_password_as(String pass) throws Throwable {
        safetyCard.loginPage = new LoginPageClass(driver);

        safetyCard.loginPage.enterPassword(pass);

        String txtINfield =  safetyCard.loginPage.password_field.getAttribute("value");
        System.out.println("The username is filled with text: "+txtINfield);

    }

    @When("^The user clicks on the login form submit button$")
    public void the_user_clicks_on_the_login_form_submit_button() throws Throwable {
        safetyCard.loginPage.login_btn.click();
        Thread.sleep(2000);
    }

    @Then("^The user is successfully logged-in and redirected to the home page$")
    public void the_user_is_successfully_logged_in_and_redirected_to_the_home_page() throws Throwable {
        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getTitle());
        safetyCard.homepage = new HomePageClass(driver);

    }

    @Then("^User`s name is presented at navigation bar as link for a dropDown menu$")
    public void user_s_name_is_presented_at_navigation_bar_as_link_for_a_dropDown_menu() throws Throwable {
        driver.findElement(By.partialLinkText(userName)).isDisplayed();

        System.out.println(driver.findElement(By.partialLinkText(userName)).getText());
        Thread.sleep(1600);

    }

    @Then("^The user is not successfully logged-in$")
    public void the_user_is_not_successfully_logged_in() throws Throwable {

        Assert.assertNotEquals(driver.getCurrentUrl(),"http://localhost:8080");




    }

    @Then("^An error message is displayed as \"([^\"]*)\"$")
    public void an_error_message_is_displayed_as(String expectedError) throws Throwable {
        Thread.sleep(777);

        WebElement errorMSG = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/form[1]/label[1]"));

        WebDriverWait wait = safetyCard.loginPage.getWait();
        wait.until(ExpectedConditions.visibilityOf(errorMSG));


        String errorMSG_txt =  driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/form[1]/label[1]")).getText();
        System.out.println(errorMSG_txt);
        //Bad credentials
        Assert.assertEquals(errorMSG_txt,expectedError);

    }

    @Given("^The user launches the SafetyCard home page with URL \"([^\"]*)\"$")
    public void the_user_launches_the_SafetyCard_home_page_with_URL(String url) throws Throwable {
       driver.get(url);
       safetyCard.homepage = new HomePageClass(driver);

    }

    @When("^The user clicks at the navigation bar login link$")
    public void the_user_clicks_at_the_navigation_bar_login_link() throws Throwable {
       safetyCard.homepage.nav_login_btn.click();

       safetyCard.loginPage = new LoginPageClass(driver);
    }

    @Then("^The user is redirected to the login page$")
    public void the_user_is_redirected_to_the_login_page() throws Throwable {
        System.out.println(driver.getCurrentUrl());
    }

    @Given("^The user launches the SafetyCard login page with URL \"([^\"]*)\" after unsuccessful login attempt$")
    public void theUserLaunchesTheSafetyCardLoginPageWithURLAfterUnsuccessfulLoginAttempt(String url) throws Throwable {
         driver.get(url);
         Assert.assertEquals(driver.getCurrentUrl(),url);
         safetyCard.loginPage = new LoginPageClass(driver);

        safetyCard.loginPage.logIN("N3TestUser","noPassword");
        Assert.assertEquals(driver.getCurrentUrl(),"http://localhost:8080/login?error");

        WebElement errorMSG = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/form[1]/label[1]"));

        WebDriverWait wait = safetyCard.loginPage.getWait();
        wait.until(ExpectedConditions.visibilityOf(errorMSG));


        String errorMSG_TXT =  driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/form[1]/label[1]")).getText();
        //Bad credentials
        Assert.assertEquals(errorMSG_TXT,"Bad credentials");

    }
}







