package SafetyCard.BDT.Runners.Offer;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions (
        features =(
                    "src/test/java/SafetyCard/BDT/Features/INScalc/ "

                    )
        ,glue = {
                "SafetyCard.BDT.StepDefinitions.Offer"
                ,"com.n3.Cucumber"
               }
       ,dryRun = false
       ,monochrome = true
       ,plugin = {

                "pretty"
                ,"html:D:\\CODES\\M6\\V1_infoCucs\\Screenshot"

                }
)

public class INScalc_Runner extends AbstractTestNGCucumberTests {
}
