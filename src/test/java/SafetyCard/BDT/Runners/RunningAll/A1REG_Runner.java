package SafetyCard.BDT.Runners.RunningAll;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
        features = "src/test/java/SafetyCard/BDT/Features/Registration/Registration.feature"
        ,glue = {
        "SafetyCard.BDT.StepDefinitions.REG"
        ,"com.n3.Cucumber"
}
        ,dryRun = false
        ,monochrome = true
        ,plugin = {

        "pretty"
        ,"json:target/report.json"

}
)

public class A1REG_Runner extends AbstractTestNGCucumberTests{
}



