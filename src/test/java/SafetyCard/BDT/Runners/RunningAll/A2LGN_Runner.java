package SafetyCard.BDT.Runners.RunningAll;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions (
        features = "src/test/java/SafetyCard/BDT/Features/Login/Login.feature"
        ,glue = {
                "SafetyCard.BDT.StepDefinitions.LGN"
                ,"com.n3.Cucumber"
               }
       ,dryRun = false
       ,monochrome = true
       ,plugin = {

        "pretty"
        ,"json:target/report.json"

                }
)

public class A2LGN_Runner extends AbstractTestNGCucumberTests {
}
