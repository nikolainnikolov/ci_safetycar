package SafetyCard.BDT.Runners.RunningAll;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = "src/test/java/SafetyCard/BDT/Features/"
        ,glue =      {
                     "SafetyCard.BDT.StepDefinitions"

                     }
        ,dryRun = false
        ,monochrome = true
        ,plugin = {

        "pretty"
        ,"json:target/report.json"

}
)

public class MusterRunner extends AbstractTestNGCucumberTests {
}
