package SafetyCard.BDT.Runners.RunningAll;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(

        features = "src/test/java/SafetyCard/BDT/Features/Offer/UserOffersManagement.feature"
        ,glue = {
                "SafetyCard.BDT.StepDefinitions.Offer"
                ,"com.n3.Cucumber"
                }
        ,dryRun = false
        ,monochrome = false
        ,plugin = {

        "pretty"
        ,"json:target/report.json"

                }
            )

public class UserOfferPage_Runner extends AbstractTestNGCucumberTests {
}
