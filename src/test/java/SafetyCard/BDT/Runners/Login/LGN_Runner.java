package SafetyCard.BDT.Runners.Login;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions (
        features = "src/test/java/SafetyCard/BDT/Features/Login/Login.feature"
        ,glue = {
                "SafetyCard.BDT.StepDefinitions.LGN"
                ,"com.n3.Cucumber"
               }
       ,dryRun = false
       ,monochrome = true
       ,plugin = {

                "pretty"
                ,"html:D:\\CODES\\M6\\V1_infoCucs\\Screenshot"

                }
)

public class LGN_Runner extends AbstractTestNGCucumberTests {
}
