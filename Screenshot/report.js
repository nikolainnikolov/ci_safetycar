$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("INScalc/Calculator.feature");
formatter.feature({
  "line": 1,
  "name": "Safety card insurance calculator",
  "description": "        As a SafetyCard user,\r\n        I want to be able to do calculate a insurance premium\r\n        so that I can use inform myself for the amount\r\n        and use this for further actions within the Website",
  "id": "safety-card-insurance-calculator",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Calculate an insurance premium by providing valid data",
  "description": "",
  "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "The user launches the SafetyCard calculator page with URL \"\u003curl\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "The user selects a car brand from a dropDown menu as \"\u003ccar\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "The user selects a car model from the dropDown menu as \"\u003cmodel\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "The user enters car cubic capacity \"\u003ccapacity\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "The user provides date for the car first registration date \"\u003cregDate\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "The user select the \u0027Had accident check\u0027 box \"\u003cisAccidentHappened\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "The user provides information for the driver`s age \"\u003cdriverAge\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "The user clicks on calculate submit button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "A car insurance is presented with total amount \"\u003cpremiumAmount\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 19,
  "name": "",
  "description": "",
  "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;",
  "rows": [
    {
      "cells": [
        "url",
        "car",
        "model",
        "capacity",
        "regDate",
        "isAccidentHappened",
        "driverAge",
        "premiumAmount"
      ],
      "line": 20,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;1"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "600",
        "11/11/2011",
        "no",
        "44",
        "443.58 BGN"
      ],
      "line": 21,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;2"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "700",
        "11/11/2011",
        "yes",
        "44",
        "532.29 BGN"
      ],
      "line": 22,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;3"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "800",
        "11/11/2011",
        "yes",
        "19",
        "554.47 BGN"
      ],
      "line": 23,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;4"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "900",
        "11/11/1999",
        "no",
        "44",
        "454.58 BGN"
      ],
      "line": 24,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;5"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1000",
        "11/11/1999",
        "yes",
        "44",
        "545.49 BGN"
      ],
      "line": 25,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;6"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1047",
        "11/11/1999",
        "yes",
        "19",
        "568.22 BGN"
      ],
      "line": 26,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;7"
    },
    {
      "comments": [
        {
          "line": 27,
          "value": "#    Category 2"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1048",
        "11/11/2011",
        "no",
        "44",
        "582.59 BGN"
      ],
      "line": 28,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;8"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1100",
        "11/11/2011",
        "yes",
        "44",
        "699.11 BGN"
      ],
      "line": 29,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;9"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1150",
        "11/11/2011",
        "yes",
        "19",
        "728.24 BGN"
      ],
      "line": 30,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;10"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1200",
        "11/11/1999",
        "no",
        "44",
        "593.59 BGN"
      ],
      "line": 31,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;11"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1250",
        "11/11/1999",
        "yes",
        "44",
        "712.31 BGN"
      ],
      "line": 32,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;12"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1309",
        "11/11/1999",
        "yes",
        "19",
        "741.99 BGN"
      ],
      "line": 33,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;13"
    },
    {
      "comments": [
        {
          "line": 34,
          "value": "#    Category 3"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1310",
        "11/11/2011",
        "no",
        "44",
        "760.06 BGN"
      ],
      "line": 35,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;14"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1470",
        "11/11/2011",
        "yes",
        "44",
        "912.07 BGN"
      ],
      "line": 36,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;15"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1500",
        "11/11/2011",
        "yes",
        "19",
        "950.07 BGN"
      ],
      "line": 37,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;16"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1680",
        "11/11/1999",
        "no",
        "44",
        "771.06 BGN"
      ],
      "line": 38,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;17"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "1709",
        "11/11/1999",
        "yes",
        "44",
        "925.27 BGN"
      ],
      "line": 39,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;18"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2356",
        "11/11/1999",
        "yes",
        "19",
        "963.82 BGN"
      ],
      "line": 40,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;19"
    },
    {
      "comments": [
        {
          "line": 41,
          "value": "#    Category 4"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2357",
        "11/11/2011",
        "no",
        "44",
        "949.15 BGN"
      ],
      "line": 42,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;20"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2400",
        "11/11/2011",
        "yes",
        "44",
        "1138.98 BGN"
      ],
      "line": 43,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;21"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2500",
        "11/11/2011",
        "yes",
        "19",
        "1186.43 BGN"
      ],
      "line": 44,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;22"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2600",
        "11/11/1999",
        "no",
        "44",
        "982.15 BGN"
      ],
      "line": 45,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;23"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2700",
        "11/11/1999",
        "yes",
        "44",
        "1178.58 BGN"
      ],
      "line": 46,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;24"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2880",
        "11/11/1999",
        "yes",
        "19",
        "1227.68 BGN"
      ],
      "line": 47,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;25"
    },
    {
      "comments": [
        {
          "line": 48,
          "value": "#    Category 5"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "2881",
        "11/11/2011",
        "no",
        "44",
        "1053.68 BGN"
      ],
      "line": 49,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;26"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "3000",
        "11/11/2011",
        "yes",
        "44",
        "1264.41 BGN"
      ],
      "line": 50,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;27"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "3200",
        "11/11/2011",
        "yes",
        "19",
        "1317.10 BGN"
      ],
      "line": 51,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;28"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "3400",
        "11/11/1999",
        "no",
        "44",
        "1086.68 BGN"
      ],
      "line": 52,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;29"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "3600",
        "11/11/1999",
        "yes",
        "44",
        "1304.01 BGN"
      ],
      "line": 53,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;30"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "4188",
        "11/11/1999",
        "yes",
        "19",
        "1358.35 BGN"
      ],
      "line": 54,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;31"
    },
    {
      "comments": [
        {
          "line": 55,
          "value": "#    Category 6"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "4189",
        "11/11/2011",
        "no",
        "44",
        "1184.28 BGN"
      ],
      "line": 56,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;32"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "4200",
        "11/11/2011",
        "yes",
        "44",
        "1421.14 BGN"
      ],
      "line": 57,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;33"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "4300",
        "11/11/2011",
        "yes",
        "19",
        "1480.35 BGN"
      ],
      "line": 58,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;34"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "4400",
        "11/11/1999",
        "no",
        "44",
        "1217.28 BGN"
      ],
      "line": 59,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;35"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "5200",
        "11/11/1999",
        "yes",
        "44",
        "1460.74 BGN"
      ],
      "line": 60,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;36"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "5497",
        "11/11/1999",
        "yes",
        "19",
        "1521.60 BGN"
      ],
      "line": 61,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;37"
    },
    {
      "comments": [
        {
          "line": 62,
          "value": "#    Category 7"
        }
      ],
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "5498",
        "11/11/2011",
        "no",
        "44",
        "1312.58 BGN"
      ],
      "line": 63,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;38"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "5821",
        "11/11/2011",
        "yes",
        "44",
        "1575.09 BGN"
      ],
      "line": 64,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;39"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "64025",
        "11/11/2011",
        "yes",
        "19",
        "1640.72 BGN"
      ],
      "line": 65,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;40"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "72537",
        "11/11/1999",
        "no",
        "44",
        "1389.58 BGN"
      ],
      "line": 66,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;41"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "80955",
        "11/11/1999",
        "yes",
        "44",
        "1667.49 BGN"
      ],
      "line": 67,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;42"
    },
    {
      "cells": [
        "http://localhost:8080/calculator",
        "Audi",
        "A4",
        "999999",
        "11/11/1999",
        "yes",
        "19",
        "1736.97 BGN"
      ],
      "line": 68,
      "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;43"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 21,
  "name": "Calculate an insurance premium by providing valid data",
  "description": "",
  "id": "safety-card-insurance-calculator;calculate-an-insurance-premium-by-providing-valid-data;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "The user launches the SafetyCard calculator page with URL \"http://localhost:8080/calculator\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "The user selects a car brand from a dropDown menu as \"Audi\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "The user selects a car model from the dropDown menu as \"A4\"",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "The user enters car cubic capacity \"600\"",
  "matchedColumns": [
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "The user provides date for the car first registration date \"11/11/2011\"",
  "matchedColumns": [
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "The user select the \u0027Had accident check\u0027 box \"no\"",
  "matchedColumns": [
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "The user provides information for the driver`s age \"44\"",
  "matchedColumns": [
    6
  ],
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "The user clicks on calculate submit button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "A car insurance is presented with total amount \"443.58 BGN\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://localhost:8080/calculator",
      "offset": 59
    }
  ],
  "location": "Calculator_stepDefinitions.the_user_launches_the_SafetyCard_calculator_page_with_URL(String)"
});
formatter.result({
  "duration": 12426735500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Audi",
      "offset": 54
    }
  ],
  "location": "Calculator_stepDefinitions.the_user_selects_a_car_brand_from_a_dropDown_menu_as(String)"
});
formatter.result({
  "duration": 1873363000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A4",
      "offset": 56
    }
  ],
  "location": "Calculator_stepDefinitions.the_user_selects_a_car_model_from_the_dropDown_menu_as(String)"
});
